# Opis in navodila za Aplikacijo Parsanje Vremena

###### V aplikaciji se najprej ob zagonu prikaže enostavni SplashActivity in nato MainActivity, v katerem so prikazana poljubna mesta, ki vsebujejo podatke o vremenu. 
###### V MainActivity-u imamo možnost iskanja določenega kraja in sortiranja, ter prikaza trenutne lokacije. Ko izberemo enega izmed krajev se nam odpre nov Activity oz. v našem primeru SelectedActivity, ki se pridobi iz podatkov pozicije, naslova, slike in opisa, kateri se mora ujemati z izbranimi podatki uporabnika. 
###### Na SelectedActivity-u se nam prikažejo sparsani podatki o samem vremenu in lokaciji, ter gumb nazaj, če se želimo vrniti na MainActivity.


###### Priložene so tudi fotografije izgleda aplikacije MainActivity, SplashActivity, SelectedActivity, searchandsort.

###### V MainActivity-u je narejen arrayList, v katerega so dodani poljubni kraji po Sloveniji, ki nam prikažejo vremenske podatke na naslednjem Activity-u. V samem MainActivity-u je dodano iskanje v katerem niz gre skozi arrayList in prikaže le ujemanje, ostale elemente arrayLista izbriše.
###### Sortiranje je prikazano najprej naraščajoče in potem padajoče (reverse), ki se primerja po naslovih v arrayListu.


###### SplashActivity traja 3 sekunde in potem preskoči na MainActivity. Ta zadeva je urejena v Manifestu, kjer lahko urejamo tudi druge Activity-e, v katerem so dodani še določeni permission-i za lokacijo preko GPS ali interneta in permission za internet, ki ga potrebujemo pri parsanju.

###### V SelectedActivity-u se parsajo podatki o vremenu. Koda najprej primerja naslov (title), da se ve na katerem Activity-u je uporabnik. Nato se izvede funkcija za ta naslov/Activity, ki pridobi podatke o vremenu. Za preskok na določen Activity se pridobijo podatki preko MyAdapterj-a in sicer naslov, ki je ključen in opis ter slika.
###### Za parsanje sem uporabil stran openweather, preko katere sem se registriral, da sem dobil API key, kateri mi je omogočil parsanje podatkov.

###### Primer parsanja, da dobimo eden JSON objekt z podatki, katere nato lahko prikažemo, je razviden iz naslednje poizvedbe https://api.openweathermap.org/data/2.5/weather?q=$CITY&units=metric&appid=$API.
###### Za CITY izberemo npr. Cankova,SI za API pa ključ, ki ga prejmemo ob registraciji. Response lahko pogledamo preko spleta, kjer je razvidno, kako so elementi gnezdeni in jih nato sparsamo.

###### Class Model nam prikazuje sestavo elementa v arrayListu, in sicer je sestavljen iz naslova, opisa in slike.

###### Preko MyAdapterj-a pridobivamo podatke, ki jih nato uporabimo v SelectedActivity-u, da vemo na kateri Activity preskočiti ob samem klicu/kliku oz. da se nam prikažejo pravilni podatki za posamično izbrano mesto v Sloveniji glede vremena.