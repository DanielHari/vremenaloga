package com.example.daniel.vreme

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row.view.*

class MyAdapter(val arrayList: ArrayList<Model>, val context: Context) : RecyclerView.Adapter<MyAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val v = LayoutInflater.from(parent.context).inflate(R.layout.row, parent, false)

        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(arrayList[position])

        holder.itemView.setOnClickListener {

          /*  if(position == 0){

            Toast.makeText(context,"klikno si prvi",Toast.LENGTH_LONG).show()

            }
            if(position == 1){

                Toast.makeText(context,"klikno si prvi plus nula",Toast.LENGTH_LONG).show()

            }
            if(position == 2){

                Toast.makeText(context,"klikno si drugi",Toast.LENGTH_LONG).show()

            }
            if(position == 3){

                Toast.makeText(context,"klikno si tretji",Toast.LENGTH_LONG).show()

            }
            if(position == 4){

                Toast.makeText(context,"klikno si cetrti",Toast.LENGTH_LONG).show()

            }*/
            //selected item position
            val model = arrayList.get(position)
            //get title and description
            var gTitle : String = model.title
            var gDescription : String = model.des
            //get image with intent
            var gImageView : Int = model.image

            //create intent
            val intent = Intent(context, SelectedActivity::class.java)


            intent.putExtra("iTitle", gTitle)
            intent.putExtra("iDescription",gDescription)
            intent.putExtra("imageView",gImageView)

            context.startActivity(intent)

        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        fun bindItem(model: Model){

            itemView.titleTv.text = model.title
            itemView.descriptionTv.text = model.des
            itemView.imageTv.setImageResource(model.image)

        }

    }
}