package com.example.daniel.vreme


import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.AsyncTask
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_selected.*
import org.json.JSONObject
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import java.util.jar.Manifest

class MainActivity : AppCompatActivity() {




    val arrayList = ArrayList<Model>()
    val displayList = ArrayList<Model>()
    lateinit var  myAdapter: MyAdapter
    lateinit var preferences: SharedPreferences



    lateinit var lm : LocationManager
    lateinit var loc : Location

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)




        if(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED
        && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_COARSE_LOCATION,android.Manifest.permission.ACCESS_FINE_LOCATION),111
            )

        lm = getSystemService(Context.LOCATION_SERVICE) as LocationManager
       // loc = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER)

        var ll = object : LocationListener{
            override fun onLocationChanged(location: Location?) {

                reverseGEOcode(location)
            }

            override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

            }

            override fun onProviderEnabled(provider: String?) {

            }

            override fun onProviderDisabled(provider: String?) {

            }



        }

        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000,100.2f,ll)

        arrayList.add(Model("Lendava","klikni gor za več info", R.drawable.cities))
        arrayList.add(Model("Murska Sobota","klikni gor za več info", R.drawable.cities))
        arrayList.add(Model("Maribor","klikni gor za več info", R.drawable.cities))
        arrayList.add(Model("Celje","klikni gor za več info", R.drawable.cities))
        arrayList.add(Model("Velenje","klikni gor za več info", R.drawable.cities))
        arrayList.add(Model("Ljubljana","klikni gor za več info", R.drawable.cities))
        arrayList.add(Model("Bled","klikni gor za več info", R.drawable.cities))
        arrayList.add(Model("Kranj","klikni gor za več info", R.drawable.cities))
        arrayList.add(Model("Slovenj Gradec","klikni gor za več info", R.drawable.cities))
        arrayList.add(Model("Jesenice","klikni gor za več info", R.drawable.cities))
        arrayList.add(Model("Koper","klikni gor za več info", R.drawable.cities))
        arrayList.add(Model("Piran","klikni gor za več info", R.drawable.cities))
        arrayList.add(Model("Izola","klikni gor za več info", R.drawable.cities))
        arrayList.add(Model("Ankaran","klikni gor za več info", R.drawable.cities))
        displayList.addAll(arrayList)


        myAdapter = MyAdapter(displayList, this)

        recyclerview.layoutManager = LinearLayoutManager(this)
        recyclerview.adapter = myAdapter


        preferences = getSharedPreferences("My_pref",Context.MODE_PRIVATE)
        val mSortSetting = preferences.getString("Sort","Ascending")

        if(mSortSetting == "Ascending"){

            sortAscending(myAdapter)


        }else if(mSortSetting == "Descending"){

            sortDescending(myAdapter)

        }
    }

    private fun sortDescending(myAdapter: MyAdapter) {

        displayList.sortWith(compareBy { it.title })
        displayList.reverse()
        myAdapter.notifyDataSetChanged()

    }

    private fun sortAscending(myAdapter: MyAdapter) {

        displayList.sortWith(compareBy { it.title })
        myAdapter.notifyDataSetChanged()


    }

    // search and sort
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {





        menuInflater.inflate(R.menu.menu,menu)


        val menuItem = menu!!.findItem(R.id.search)
        val menuItemSort = menu!!.findItem(R.id.sorting)



        if(menuItem != null){

            val searchView = menuItem.actionView as SearchView

            val editText = searchView.findViewById<EditText>(androidx.appcompat.R.id.search_src_text)
            editText.hint = "Search..."


            searchView.setOnQueryTextListener(object  : SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return  true
                }

                override fun onQueryTextChange(newText: String?): Boolean {

                    if(newText!!.isNotEmpty()){
                        displayList.clear()
                        val search = newText.toLowerCase(Locale.getDefault())
                        arrayList.forEach {
                            if(it.title.toLowerCase(Locale.getDefault()).contains(search)){

                                displayList.add(it)
                            }
                        }

                        recyclerview.adapter!!.notifyDataSetChanged()

                    }else{

                        displayList.clear()
                        displayList.addAll(arrayList)
                        recyclerview.adapter!!.notifyDataSetChanged()

                    }

                    return  true
                }


            })

        }

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {


        val id = item!!.itemId
        if(id==R.id.sorting){

            sortDialog()

        }

        return super.onOptionsItemSelected(item)
    }

    private fun sortDialog() {
        val options = arrayOf("Ascending","Descending")
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Sort By")
        builder.setIcon(R.drawable.ic_sort_black_24dp)


        builder.setItems(options){dialog, which ->

            if(which == 0){

                val editor : SharedPreferences.Editor = preferences.edit()
                editor.putString("Sort","Ascending")
                editor.apply()
                sortAscending(myAdapter)
                Toast.makeText(this@MainActivity,"Ascending order",Toast.LENGTH_LONG).show()

            }
            if(which == 1) {
                val editor: SharedPreferences.Editor = preferences.edit()
                editor.putString("Sort", "Descending")
                editor.apply()
                sortDescending(myAdapter)
                Toast.makeText(this@MainActivity, "Descending order", Toast.LENGTH_LONG).show()



                }
        }

        builder.create().show()





    }


    private fun reverseGEOcode(location: Location?) {

        var gc = Geocoder(this,Locale.getDefault())
        var addresses = gc.getFromLocation(location!!.latitude,location.longitude,2)
        var address = addresses.get(0)
        Toast.makeText(this,"trenutna lokacija \n latitude: " + address.latitude + "\n longitude: "+ address.longitude,Toast.LENGTH_LONG).show()


    }



}



